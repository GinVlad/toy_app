class User < ActiveRecord::Base #Lớp User tương ứng với table Users và được kế thừa bởi lớp ActiveRecord
    has_many :microposts #Tạo mối liên hệ giữ Users và Micropost thông qua model micropost. 1 user có thể có nhiều microposts
    validates :name, presence: true #validates :name - kiểm tra biến name, và không cho rỗng
    validates :email, presence: true #validates :email - kiểm tra biến email, và không cho rỗng
end
