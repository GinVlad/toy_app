class Micropost < ActiveRecord::Base #Lớp Micropost tương ứng với table Microposts và được kế thừa bởi lớp ActiveRecord
    validates :content, length: {maximum: 140}, presence: true #Kiểm tra độ dài của column content, giới hạn độ dài lớn nhất là 140 ký tự. Kiểm tra với presence: true để xác định phần content không được trống
    belongs_to :user #Tạo mối quan hệ giữa Users và Micropost thông qua model user, Micropost thuộc về User thông qua user_id
end
